document.addEventListener('DOMContentLoaded', function() {
    const tabsContainer = document.querySelector('.tabs');
    const tabs = document.querySelectorAll('.tabs-title');
    const contents = document.querySelectorAll('.tabs-content li');

    // Функція для деактивації всіх вкладок та приховування вмісту
    function deactivateAll() {
        tabs.forEach(tab => tab.classList.remove('active'));
        contents.forEach(content => content.classList.remove('active'));
    }

    // Обробник подій для делегування на вкладках
    tabsContainer.addEventListener('click', function(event) {
        if (event.target.classList.contains('tabs-title')) {
            const tabNumber = event.target.getAttribute('data-tab');
            
            deactivateAll();
            
            event.target.classList.add('active');
            document.querySelector(`.tabs-content li[data-content="${tabNumber}"]`).classList.add('active');
        }
    });

    // Встановлення початкового стану (перша вкладка активна)
    tabs[0].classList.add('active');
    contents[0].classList.add('active');
});

  